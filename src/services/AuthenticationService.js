import Axios from "axios";
import Router from '../router'

const API_URL = process.env.API_URL ? process.env.API_URL : 'http://localhost:8000/api/';

export default class AuthenticationService {
  static isLogedIn() {
    const token = localStorage.getItem("token");
    return token ? true : false
  }

  static register(data) {
    return Axios({
        method: 'post',
        url: API_URL+"register",
        data: data,
    })
  }
  
  static login(data) {
    return Axios({
        method: 'post',
        url: API_URL+"login",
        data: data,
    })
  }

  static logout() {
    // remove token from localStorage to log user out
    localStorage.removeItem('token');
    Router.go('/login')
  }  
}
