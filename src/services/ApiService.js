import Axios from "axios";

const API_URL =  process.env.API_URL ? process.env.API_URL : 'http://localhost:8000/api';

export default class ApiService {
    static get(path, data = {}) {        
        return Axios({
            method: 'get',
            url: API_URL+path,
            params: data,
            headers: this.headers()
        })
    }
    
    static post(path, data = {}) {
        return Axios({
            method: 'post',
            url: API_URL+path,
            data: data,
            headers: this.headers()
        })
    }

    static put(path, data = {}) {
        return Axios({
            method: 'put',
            url: API_URL+path,
            data: data,
            headers: this.headers()
        })
    }

    static delete(path, data = {}) {
        return Axios({
            method: 'delete',
            url: API_URL+path,
            params: data,
            headers: this.headers()

        })
    }

    static headers() {
        const authHeader = localStorage.getItem('token')
            ? { Authorization: "Bearer " + JSON.parse(localStorage.getItem('token'))}
            : {};

        return {
            ...authHeader,
            "Content-Type": "application/json"
        }
    }
}