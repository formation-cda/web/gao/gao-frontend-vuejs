import moment from "moment"
import EventBus from '../eventBus'

export default class Utils {
    static handleResponse(response) {
        return {success: response.data.success, data: response.data.data, message: response.data.message}
    }

    static formatName(lastname="", firstname=""){
        const formatedName = (
                                firstname 
                                ? firstname.substring(0,1).toUpperCase()+firstname.substring(1,(firstname.length)) 
                                : ""
                             )+" "+
                             (
                                (firstname && lastname) 
                                ? lastname.substring(0,1).toUpperCase()+"." 
                                :  
                                (
                                    (!firstname && lastname) 
                                    ? lastname.substring(0).toUpperCase()
                                    : ""                                     
                                )
                              )     
        return formatedName
    }

    static formatDate(date=null){
        return date ? moment(date).format("DD-MM-YYYY H:m:s") : "Aucune date"
    }

    static emitMethod (event_name, payLoad) {
        EventBus.$emit(event_name, payLoad);
    }

    static eBusOnMethod(event_name, callback){
        EventBus.$on(event_name, callback);
    }
}