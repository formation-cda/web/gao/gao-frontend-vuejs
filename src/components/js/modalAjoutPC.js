export default {
  props: {
    dialog: {}
  },  
  data(){
    return {
        loading: false,    
        posteName: '',
        posteNameRules: [
          v => !!v || 'Nom de poste requis',
        ]
    }  
  },
  methods: {
    close () {
        this.posteName = ''
        this.$emit('update:dialog', false)
    },
    submitForm () {
        const isValid = this.$refs.form.validate()
        if(isValid) {
            this.$emit('call', this.posteName ) 
        }
    },
  }
}