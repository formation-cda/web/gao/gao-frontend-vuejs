
import modalAddAttribution from "../views/ModalAddAttribution.vue"
import ApiService from '../../services/ApiService'
import Utils from '../../helpers/Utils'
import EventBusEnum from '../../enum/EventBusEnum'

export default {
    components:{
        modalAddAttribution,
    },
    props: {
        ordiname:{},
        dataAttribution:{},
        selectedDate: {}
    },
    created() {
        this.initialize()
        this.displayHoraire()
    },
    data() {
        return {
            attributions:[],
            horaire:[],
            dialog:false,
            time: "",
            date: "",
            idPc: "",
            dataPoste: this.dataAttribution
        }
    },
    methods:{
        initialize(){   
            this.horaire = []
            this.attributions = []
            this.dataPoste?.attributions?.map(dataAttrib => {
                const horaire = dataAttrib?.horaire
                this.attributions.push({
                    [horaire]:{
                        "idPc":this.dataPoste?.id,
                        "idAttrib":dataAttrib?.id,
                        "nom":dataAttrib?.client?.lastName,
                        "prenom":dataAttrib?.client?.firstName
                    }     
                })
            }) 

        },
        displayHoraire(){

            const arrHoraire = ['8','9','10','11','12','13','14','15','16','17','18']
            
            arrHoraire.map((heure, index) => {    
                let attribution = {
                    "idPc":this.dataPoste?.id,
                    "idAttrib":"",
                    "heure": heure,
                    "nom":"",
                    "prenom":""
                }          

                let attribFound = false

                this.attributions.map( attrib =>{  
                    if(Object.keys(attrib) == heure && !attribFound){
                        attribution = {
                            "idPc":attrib?.[heure]?.idPc,
                            "idAttrib":attrib?.[heure]?.idAttrib,
                            "heure": heure,
                            "nom": attrib?.[heure]?.nom,
                            "prenom": attrib?.[heure]?.prenom
                        }     
                        attribFound = true                   
                    }   
                })              

                this.horaire.push(attribution)
            })          

        },
        passData(dialog, time, idPc){
            this.dialog = dialog
            this.time = time
            this.date = this.selectedDate
            this.idPc = idPc
        },
        removeAttrib(attribId, idPc) {
            const data = {id: attribId}
            ApiService.post('/attributions/del',data)
            .then(Utils.handleResponse)
            .then(response => {
                const deleteMessage = response.message
                if(response.success) {
                    const arrAtrib = this.dataPoste.attributions.filter(elem => elem.id !== attribId)
                    this.dataPoste.attributions = arrAtrib
                    this.initialize()
                    this.displayHoraire() 
                    Utils.emitMethod(EventBusEnum.EVENT_SNACKBAR, {timeout:3000, message: deleteMessage, show: true, color: 'green'})
                } else {
                    Utils.emitMethod(EventBusEnum.EVENT_SNACKBAR, {timeout:3000, message: deleteMessage, show: true, color: 'red'})                    
                }
            })
        },
        addAttribution: function (data) {  
            ApiService.post('/attributions', data)
            .then(Utils.handleResponse)
            .then(response => {
                const attributionMessage = response.message
                if(response.success) {
                    this.dataPoste.attributions.push(response.data)
                    this.initialize()
                    this.displayHoraire() 
                    Utils.emitMethod(EventBusEnum.EVENT_SNACKBAR, {timeout:3000, message: attributionMessage, show: true, color: 'green'})
                } else {
                    Utils.emitMethod(EventBusEnum.EVENT_SNACKBAR, {timeout:3000, message: attributionMessage, show: true, color: 'red'})                    
                }
            })
        },
    }
}

