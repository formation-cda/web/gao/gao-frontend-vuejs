import ApiService from '../../services/ApiService'
import Utils from '../../helpers/Utils'
import EventBusEnum from '../../enum/EventBusEnum'

export default {
  props: {
    idPcM: {},
    selectedDateM: {},
    selectedTime: {},
    dialog: {}
  },  
  data(){
    return {
        loading: false,
        items: [],
        search: null,
        select: null,
        idClient: '',  
        selectedClient: '',
        isNewClient: false,
        isClient: false,
    }
  },
  watch: {
    search: async function (val) {
      if (val && val.length > 1) {
          this.client = val
          this.loading = true
          ApiService.post('/clients/search',{word: val})
          .then(Utils.handleResponse)
          .then(response => {
            if(response && response.success) {
              this.loading = false;
              if(response.data.length > 0) {   
                this.isNewClient = false   
                this.isClient = true               
                response.data.forEach(dat => {
                  this.items.push(this.formatClient(dat))
                });
              } else if(!this.selectedClient?.id){
                this.isNewClient = true
                this.isClient = false     
              }
            }
          })
      } else {
        this.isNewClient = false 
        this.isClient = false     
      }
    },
  },
  methods: {
    checkUpdatedInput (val) {
      if(val && val.id) {
        this.isClient = true
      } else {
        this.isClient = false
      }
    },
    close() { 
      this.search = null
      this.selectedClient = ''
      this.items = []
      this.$emit('update:dialog', false)
    },
    addAttrib(){
      this.$emit('select', {clientId: this.selectedClient.id, posteId: this.idPcM, date: this.selectedDateM, horaire: this.selectedTime})
      this.close()
    },
    formatClient(client){
      return {
        composed: (client.firstName ? client.firstName : '')+' '+client.lastName,
        id: client.id
      }
    },
    extractFirstNameAndLastName(client) {
      const parts = client.split(' ');
      const lastName = parts.pop();
      const firstName = parts.join(' ')
      return {firstName, lastName};
    },
    addClient() {
      const data = this.extractFirstNameAndLastName(this.client) 
      ApiService.post('/clients', data)
      .then(Utils.handleResponse)
      .then(response => {
        if(response.success) {
          this.search = this.formatClient(response.data).composed
          this.selectedClient = response.data
          this.items.push(this.formatClient(response.data))
          this.isClient = true
          Utils.emitMethod(EventBusEnum.EVENT_SNACKBAR, {timeout:3000, message: response.message, show: true, color: 'green'})
        } else {
          Utils.emitMethod(EventBusEnum.EVENT_SNACKBAR, {timeout:3000, message: response.message, show: true, color: 'red'})
        }
      })      
    }
  }
}